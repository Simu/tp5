#ifndef _RESSOURCE_H_
#define _RESSOURCE_H_

#include "Case.hpp"

class Ressource : public Case
{
	unsigned int nbRessource;

	public :
		Ressource();
		unsigned int getNbRessource();
		void setNbRessource(unsigned int);
		void evolutionRessource();
		void afficher();
};

#endif