#include "Case.hpp"
#include <iostream>

unsigned int Case::nbCase = 0;

Case::Case() : id(nbCase), pheromone(0)
{
	nbCase++;
}

Case::~Case()
{

}
unsigned int Case::getNbCase()
{
	return nbCase;
}

unsigned int Case::getId()
{
	return id;
}

unsigned int Case::getPheromone()
{
	return pheromone;
}

void Case::setPheromone(unsigned int inPheromone)
{
	pheromone = inPheromone;
}

std::vector<Fourmi *> & Case::getVecteurFourmi()
{
	return vecteurFourmi;
}

void Case::afficher()
{
	std::cout << " . ";
}