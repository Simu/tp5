#ifndef _GRILLE_H_
#define _GRILLE_H_

#include <vector>
#include "Case.hpp"


class Grille
{
	unsigned int nbLignes;
	unsigned int nbColonnes;
	std::vector< std::vector<Case*> > vecteurCase;

	public :
		Grille(unsigned int, unsigned int);
		unsigned int getNbLignes();
		unsigned int getNbColonnes();
		Case * & getCase(unsigned int);
		Case * & getCase(unsigned int, unsigned int);
		void afficher();
};

#endif