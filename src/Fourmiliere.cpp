#include "Fourmiliere.hpp"
#include <iostream>

Fourmiliere::Fourmiliere(unsigned int inNbRessourceStock, unsigned int inIdColonie) 
	:	nbRessourceStock(inNbRessourceStock)
	,	idColonie(inIdColonie)
{
}

unsigned int Fourmiliere::getIdColonie()
{
	return idColonie;
}

unsigned int Fourmiliere::getNbRessourceStock()
{
	return nbRessourceStock;
}

void Fourmiliere::setNbRessourceStock(unsigned int inNbRessourceStock)
{
	nbRessourceStock = inNbRessourceStock;
}

void Fourmiliere::afficher()
{
	(idColonie == 1) ? std::cout << "\033[1;31m F \033[0m" : std::cout << "\033[1;34m F \033[0m" ;
}