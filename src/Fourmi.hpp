#ifndef FOURMI_HPP
#define FOURMI_HPP

#include <iostream>
#include <string.h>


class Fourmi {

	protected :
		unsigned int age ;
		unsigned int idColonie ;
		unsigned int idCase ;
		unsigned int id ;
		bool adulte ;
		static unsigned int nbFourmis ;
		
	public :
		Fourmi() ;
		virtual ~Fourmi() ;

		unsigned int getAge() ; 
		void setAge( unsigned int ) ; 
		unsigned int getIdColonie() ; 
		bool getAdulte() ; 
		void setAdulte( bool ) ; 
		unsigned int getIdCase() ; 
		void setIdCase( unsigned int ) ;
		unsigned int getId() ; 	
		static unsigned int getCompteur() ;
		virtual void action() ;
		virtual void deplacement() ;	

} ; 


#endif
