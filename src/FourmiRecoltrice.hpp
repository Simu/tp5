#ifndef FOURMIRECOLTRICE_HPP
#define FOURMIRECOLTRICE_HPP

#include <iostream>
#include <string.h>
#include "Fourmi.hpp"
#include "Grille.hpp"


class FourmiRecoltrice : public Fourmi
{
	private :
		unsigned int nbRessourceTransportee ;

	public :
		FourmiRecoltrice() ; //gerer le fait qu ça soit des larves
		~FourmiRecoltrice() ;
		void deplacement() ;
		void action() ;
		unsigned int getNbressourceTransportee() ;
		void seNbResourceTransportee( unsigned int ) ;
		void mort( Grille & ) ; 

} ;

#endif
