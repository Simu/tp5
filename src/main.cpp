#include "Grille.hpp"
#include "mt19937ar.hpp"
#include "Fourmiliere.hpp"
#include "FourmiRecoltrice.hpp"

void runSimu(unsigned int nbIterationMax, Grille & grille)
{
	unsigned int nbIteration = 0;
	int i , j ;
	FourmiRecoltrice f ;
	(grille.getCase(190))->getVecteurFourmi().push_back(&f) ;
	int idCase = 190 ;
	while (nbIteration <= nbIterationMax)
	{
		system("clear");
		for(i=0;i<20;i++){
			for(j=0;j<20;j++){
				if (  idCase == i * 20 + j ) 
				{
					unsigned int colonnes = grille.getNbColonnes();
					unsigned int lignes = grille.getNbLignes();
					int n = rand() % 4 ;

					switch (n) 
					{
						case 0 : 
						{
		
								(grille.getCase(idCase+1))->getVecteurFourmi().push_back(&f) ;
								(grille.getCase(idCase))->getVecteurFourmi().pop_back() ;
								idCase = idCase+1 ; 
							
							break;
						}
						case 1 : {

								(grille.getCase(idCase-1))->getVecteurFourmi().push_back(&f) ;
								(grille.getCase(idCase))->getVecteurFourmi().pop_back() ;
								idCase = idCase-1 ; 
							
						} ; break ;

						case 2 : {

								(grille.getCase(idCase + colonnes))->getVecteurFourmi().push_back(&f) ;
								(grille.getCase(idCase))->getVecteurFourmi().pop_back() ;
								idCase = idCase+colonnes ; 
							
						} ; break ;
						case 3 : {
						
								(grille.getCase(idCase - colonnes))->getVecteurFourmi().push_back(&f) ;
								(grille.getCase(idCase))->getVecteurFourmi().pop_back() ;
								idCase = idCase - colonnes ;
							
						} ; break ;
					}
				}
			}
		}
		grille.afficher();
		system("sleep 1");

		nbIteration++;
	}
}
	

int main()
{
	unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
 	init_by_array(init, length);

	Grille g(20, 20);
	runSimu(5, g);

	return 0;
}