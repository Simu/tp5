#ifndef FOURMIREINE_H
#define FOURMIREINE_H

#include <iostream>
#include <string.h>
#include "Fourmi.hpp"
#include "Grille.hpp"
#include <cstdlib>


class FourmiReine : public Fourmi
{
	private :

	public :
		FourmiReine() ;
		~FourmiReine() ;
		void donneNaissance( unsigned int , Grille & ) ;
		void mort(Grille &) ;
		void deplacement() ;
		void action( ) ;
} ;

#endif
