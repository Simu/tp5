#ifndef FOURMISOLDAT_H
#define FOURMISOLDAT_H

#include <iostream>
#include <string.h>
#include "Fourmi.hpp"
#include "Grille.hpp"


class FourmiSoldat : public Fourmi
{
	private :

	public :
		FourmiSoldat();
		~FourmiSoldat();
		void deplacement(unsigned int, Grille &, FourmiSoldat);
		void mort();
		void action();
};

#endif
