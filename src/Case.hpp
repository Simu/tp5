#ifndef _CASE_H_
#define _CASE_H_

#include<vector>
#include "Fourmi.hpp"

class Case
{
	protected :
		static unsigned int nbCase;
		unsigned int id;
		unsigned int pheromone;
		std::vector<Fourmi *> vecteurFourmi;

	public :
		Case();
		virtual ~Case();
		static unsigned int getNbCase();
		unsigned int getId();
		unsigned int getPheromone();
		void setPheromone(unsigned int);
		std::vector<Fourmi *> & getVecteurFourmi();
		virtual void afficher();

};

#endif
