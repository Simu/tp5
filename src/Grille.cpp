#include "Grille.hpp"
#include "Case.hpp"
#include "Fourmi.hpp"
#include "Fourmiliere.hpp"
#include "Ressource.hpp"
#include "mt19937ar.hpp"
#include <vector>

Grille::Grille(unsigned int inNbLignes, unsigned int inNbColonnes)
{
	nbLignes = inNbLignes;
	nbColonnes = inNbColonnes;
	vecteurCase.resize(nbLignes);
	for (unsigned int i = 0; i < vecteurCase.size(); ++i)
	{
		vecteurCase[i].resize(nbColonnes);
	}

	unsigned int idf1 = genrand_real2() * nbLignes * nbColonnes;
	unsigned int idf2 = genrand_real2() * nbLignes * nbColonnes;
	unsigned int idColonie = 1;
	for (unsigned int i = 0; i < nbLignes * nbColonnes; ++i)
	{
		if (i == idf1 || i == idf2)
		{
			getCase(i) = new Fourmiliere(100, idColonie);
			idColonie++;
		}
		else
		{
			double p = genrand_real2();
			if (p < 0.1)
			{
				getCase(i) = new Ressource;
			}
			else
			{
				getCase(i) = new Case;
			}
		}
	}
}

unsigned int Grille::getNbLignes()
{
	return nbLignes;
}

unsigned int Grille::getNbColonnes()
{
	return nbColonnes;
}

Case * & Grille::getCase(unsigned int id)
{
	return  (vecteurCase[id / nbColonnes])[id % nbColonnes];
}

Case * & Grille::getCase(unsigned int idLigne, unsigned int idColonne)
{
	return (vecteurCase[idLigne])[idColonne];
}

void Grille::afficher()
{
	std::cout << "=============== GRILLE DE LA SIMULATION ===============" << std::endl << std::endl;
	for (unsigned int i = 0; i < nbLignes; ++i)
	{
		for (unsigned int j = 0; j < nbColonnes; ++j)
		{
			if (getCase(i,j)->getVecteurFourmi().size() == 0)
			{
				getCase(i,j)->afficher();
			}
			else
			{
				if (getCase(i,j)->getVecteurFourmi()[0]->getIdColonie() == 1)
				{
					std::cout << "\033[1;31m " << getCase(i,j)->getVecteurFourmi().size() << " \033[0m";
				}
				else
				{
					std::cout << "\033[1;34m " << getCase(i,j)->getVecteurFourmi().size() << " \033[0m";
				}
			}
		}
		std::cout << std::endl;
	}
	std::cout << "============ INFORMATION SUR LA SIMULATION ============" << std::endl << std::endl;
}
