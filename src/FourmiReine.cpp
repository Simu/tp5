#include "FourmiReine.hpp"
#include "FourmiRecoltrice.hpp"
#include "FourmiSoldat.hpp"
#include "mt19937ar.hpp"

int nbMaxOeufs ;


FourmiReine::FourmiReine() {
}

FourmiReine::~FourmiReine() {
}


void FourmiReine::donneNaissance(unsigned int idCase, Grille & g)
{
	int n = genrand_real2() * nbMaxOeufs;
	for (int i = 0; i < n; i++) {
		double p = genrand_real2();
		if (p < 0.5) 
		{
			(g.getCase(idCase))->getVecteurFourmi().push_back(new FourmiRecoltrice);
			
		} 
		else 
		{
			(g.getCase(idCase))->getVecteurFourmi().push_back(new FourmiSoldat);
		}
	}	
}


void FourmiReine::mort( Grille & g ) {
}

void FourmiReine::deplacement() {
}


void FourmiReine::action() {	
}
