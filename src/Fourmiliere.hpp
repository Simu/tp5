#ifndef _FOURMILIERE_H
#define _FOURMILIERE_H

#include "Case.hpp"

class Fourmiliere : public Case
{
	unsigned int nbRessourceStock;
	unsigned int idColonie;
	public :
		Fourmiliere(unsigned int, unsigned int);
		unsigned int getIdColonie();
		unsigned int getNbRessourceStock();
		void setNbRessourceStock(unsigned int);
		void afficher();
};

#endif