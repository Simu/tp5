#include "Ressource.hpp"
#include "mt19937ar.hpp"

#include <iostream>

Ressource::Ressource()
{
	double p = genrand_real2();
	if (p < 0.45)
	{
		if (p < 0.15)
		{
			nbRessource = 5;
		}
		else
		{
			nbRessource = 3;
		}
	}
	else
	{
		nbRessource = 0;
	}

}

unsigned int Ressource::getNbRessource()
{
	return nbRessource;
}

void Ressource::setNbRessource(unsigned int inNbRessource)
{
	nbRessource = inNbRessource;
}

void Ressource::evolutionRessource()
{

}

void Ressource::afficher()
{
	std::cout << " " << nbRessource << " ";
}