#SRC=Chaine.cpp tests_catch.cpp main_test.cpp
SRC=$(wildcard src/*.cpp)  
EXE=tp5

CXXFLAGS+=-Wall -Wextra -MMD -ansi -pedantic -g -O2 -std=c++11
LDFLAGS= 

OBJ=$(addprefix build/,$(SRC:src/%.cpp=%.o))
DEP=$(addprefix build/,$(SRC:.cpp=.d))

all: $(OBJ)
	$(CXX) -o $(EXE) $^ $(LDFLAGS)

build/%.o: src/%.cpp 
	@mkdir -p build
	$(CXX) $(CXXFLAGS) -o $@ -c $<

clean:
	rm -rf build core *.gch

-include $(DEP)
